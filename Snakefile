from collections import defaultdict
from pathlib import Path
import re

import pandas

info = pandas.read_csv('CTE_sample_info_raw.tsv',sep='\t')

# the sequencing sample names have a TS- prepended
info.SampleName = ['TS-{}'.format(_) for _ in info.SampleName]
status_map = {'RHI':'RHIN','CTE_12':'CTEM','CTE_34':'CTES','CNT':'CNTL'}

# concatenated fastq
run_dirs = (
    '2018-07-25_Stein-B1S1',
    '2018-07-27_Stein-B1S2',
    '2018-07-30_Stein-B2S1',
    '2018-08-01_Stein-B2S2',
    '2018-08-13_Stein-B3S1',
    '2018-08-15_Stein-B3S2',
    '2018-08-20_Stein-B3S3',
    '2018-08-21_Stein-B4S1'
)
source_paths = expand('/projectnb/bubhub/data/stein/{dir}/fastq',dir=run_dirs)
source_paths = [Path(_) for _ in source_paths]
sample_fastqs = defaultdict(list)
concat_fastqs = []
for k, sample in info.iterrows() :
    name = sample.SampleName
    status = sample.Status
    ID = '{status}_{id:04d}_RNASeq_BA9_polyA'.format(status=status_map[status],id=k)
    info.loc[k,'ID'] = ID
    for path in source_paths:
        for fn in path.glob('**/{}_*.fastq.gz'.format(name)) :
            read = re.search('_(R[12])_',fn.name).group(1)
            sample_fastqs[(ID,read)].append(str(fn))

    for read in ('R1','R2') :
        sample_fastqs[(ID,read)] = list(sorted(sample_fastqs[(ID,read)]))

    assert len(sample_fastqs[(ID,'R1')]) == len(sample_fastqs[(ID,'R2')])

    # don't have some samples yet
    if len(sample_fastqs[(ID,'R1')]) == 0 :
        print('Warning! did not find sequences for sample {}'.format(name))
        continue

    concat_fastqs.extend(
        expand('cte_mRNASeq/{ID}__{read}.fastq.gz',
        ID=ID,
        read=('R1','R2'))
    )
# put the ID column first
info = info[['ID']+info.columns[:-1].tolist()]
info.to_csv('CTE_sample_info.csv',index=False)

rule all:
    input:
        concat_fastqs,
        'CTE_sample_info_all.csv'

rule concat:
    input: lambda w: sample_fastqs[(w.ID,w.read)]
    output: 'cte_mRNASeq/{ID}__{read}.fastq.gz'
    shell:
        'zcat {input} | gzip -c > {output}'

rule combined_sample_info:
    input:
        info='CTE_sample_info.csv',
        markers='RNASeq_cases_cell_type_markers.csv',
        covariates='RNASeqAnalysis_wDementiahx.csv'
    output:
        'CTE_sample_info_all.csv'
    run:
        import pandas as pd
        import numpy as np

        cell_markers = pd.read_csv(input.markers,sep=',')

        CTE_info = pd.read_csv(input.info, sep=',', usecols=['ID','Status','SampleName','RIN', 'Seq_Batch'])

        print(len(CTE_info), len(cell_markers))
        CTE_info['SampleName'] = CTE_info['SampleName'].str.replace('TS-','')
        CTE_info = pd.merge(CTE_info, cell_markers, left_on='SampleName', right_on='Case ID')
        print(len(CTE_info))

        CTE_info['SampleName']=['{}-{:04d}'.format(_.split('-')[0], int(_.split('-')[1])) if 'K' in _ else _ for _ in CTE_info['SampleName']]
        CTE_info['SampleName']=[ _.replace('-','') if 'BVAX' in _ else _ for _ in CTE_info['SampleName']]

        CTE_info['logAT8'] = np.log10(CTE_info.AT8)

        CTE_info['ID'] = CTE_info['ID'].str.split('_').str[0] + '_' + CTE_info['ID'].str.split('_').str[1]

        u_info = pd.read_csv(input.covariates, sep=',', 
                             usecols=['subjid', 'agedeath', 'CTEstage', 'DementiaHx','PathAD','PathLBD'
                                     ,'agecogsx','afe' ,'totyrs' ,'Race_Self_Report','sport','footyrs'])
        u_info['subjid'] =['BVAX{:03d}'.format(int(_.split('BVAX')[1])) if 'BVAX' in _ else _ for _ in u_info['subjid']]

        info = pd.merge(CTE_info, u_info, left_on='SampleName', right_on='subjid', how='left')
        info['PathLBD'] = [ '1' if _ == '0' else _ for _ in info['PathLBD']]
        info = info.replace(r'\s+|^$', np.nan, regex=True)
        info = info.replace(r'unk', np.nan, regex=True)
        cols = list(info)[6:]
        info[cols] = info[cols].apply(pd.to_numeric, errors='coerce')

        info = info.drop(columns=['Case ID','subjid'])
        # Drop ,'SampleName', 'subjid'
        # Make changes
        info.loc[30,'ID'] = 'CTEM_0031'
        info.loc[30,'Status'] = 'CTE_12'
        info.loc[30,'Diagnosis'] = 'CTEM'
        info.loc[54,'ID'] = 'CTES_0055'
        info.loc[54,'Status'] = 'CTE_34'
        info.loc[54,'Diagnosis'] = 'CTES'
        info2 = info.copy()

        info.to_csv(output[0], index=False)
