# Last edit: Apr 21 2020

General file name breakdown:

<<<<<<< HEAD
DE FILES:
{analysis}_{subs} DE_{last De covariate}_annot.csv  (annot - has gene names on them)  
e.g. CTEM_RHIN_NOtot_DE_totyrs_annot.csv, analysis: CTEM_RHIN on samples with NO OUTLIERS (NO), totyrs covariate (agedeath + RIN + batch is already included if the covariate is not ARS)  

FGSEA FILES:
{analysis}_{subs} DE_{De covariate}_annot_fgsea_{gmt}_{L2FC cov used for FGSEA}.csv  
e.g. all_wocnt_NOtot_de_totyrs_annot_fgsea_c2cp_totyrs.csv  

ANALYSES:
- CTE_34: CTES  
- all_wocnt: all samples (excluding controls)  
- all_rhin: all samples (excluding controls), has status in DE covariate (referenced RHIN)  
- CTEM_RHIN  
- CTES_RHIN  
- CTES_CTEM  

SUBS:
- all - all samples in the specified analysis (e.g. CTEM_RHIN only has CTEM + RHIN samples)  
- NOtot - no outliers - only used in totyrs so far  

Covariates:
- AT8  
- totyrs  
- ARS - age death + RIN + seq_batch (no other covariates in)  
- noagetotyrs - RIN + seqbatch + totyrs  

Combined files (has all analysis in):  
- Merged df script not currently in snakefile  
- all_DE_totyrs_combined_annot.csv  
- all_DE_AT8_annot.csv  

Result files at a glance:
- commandline generating these files: python de_report.py  
- report_de.csv  
- report_fgsea.csv  
=======
DE FILES: `{analysis}_{subs}_DE_{last De covariate}_annot.csv`  (annot - has gene names on them)
e.g. `CTEM_RHIN_NOtot_DE_totyrs_annot.csv`, analysis:

 * `CTEM_RHIN` on samples
 * with NO OUTLIERS (`NOtot`),
 * `totyrs` covariate (agedeath + RIN + batch is already included if the covariate is not ARS)

FGSEA FILES:
`{analysis}_{subs} DE_{De covariate}_annot_fgsea_{gmt}_{L2FC cov used for FGSEA}.csv`
e.g. `all_wocnt_NOtot_de_totyrs_annot_fgsea_c2cp_totyrs.csv`

ANALYSES:

 * `CTE_34`: CTES
 * `all_wocnt`: all samples (excluding controls)
 * `all_rhin`: all samples (excluding controls), has status in DE covariate (referenced RHIN)
 * `CTEM_RHIN`
 * `CTES_RHIN`
 * `CTES_CTEM`

SUBS:

 * `all` - all samples in the specified analysis (e.g. `CTEM_RHIN` only has CTEM + RHIN samples)
 * `NOtot` - no outliers - only used in totyrs so far

Covariates:

 * `AT8`
 * `totyrs`
 * `ARS` - age death + RIN + seq_batch (no other covariates in)
 * `noagetotyrs` - RIN + seqbatch + totyrs

Combined files (has all analysis in) :

 * Merged df script not currently in snakefile
 * `all_DE_totyrs_combined_annot.csv`
 * `all_DE_AT8_annot.csv`

Result files at a glance:
commandline generating these files: `python de_report.py`

 * `report_de.csv`
 * `report_fgsea.csv`
>>>>>>> c9c9f9a667d4e30ac04681aab300a7ab439d2908

# Totyrs files
script generating these files: `de_snake`

# AT8 files
script generating these files: `de_snake`


