#!/usr/bin/env python
import pandas as pd
import glob
import csv

def to_print(filenames):
    de = {}
    fgsea = {}
    with open('report_de.csv','wt') as d:
        d = csv.writer(d)
        d.writerow(['Comparison','Group','DE_cov','L2FC_col' ,'pvalue < 0.05', 'padj < 0.05', 'Pos_L2FC padj < 0.05', 'Neg_L2FC padj < 0.05','pval<0.1','padj<0.01','padj<0.1'])
        with open('report_fgsea.csv', 'wt') as f:
            f = csv.writer(f)
            f.writerow(['Comparison','Group','DE cov','L2FC','gmt' ,'padj < 0.05', 'padj < 0.1', 'Pos NES padj < 0.05', 'Neg NES padj < 0.05'])
            for name in filenames:
                df = pd.read_csv(name,sep=',')
                columns = [name.split('_')[4]]
                name = name.replace('.csv','')
                if name.split('_')[0] == 'BA9':
                    columns = ['AGE']
                if columns[0] == 'AtD' or columns[0] == 'NAtD' or columns[0] == 'ND':
                    columns = ['DementiaHx']
                if columns[0] == 'noagetotyrs' or columns[0] == 'AtT':
                    columns = ['totyrs']
                if columns[0] == 'ARS':
                    columns = ['agedeath']
                if columns[0] == 'NAt':
                    columns = ['NeuN','AT8']

                for column in columns:
                    samp, subs, cov = name.split('_')[0] + '_' + name.split('_')[1], name.split('_')[2], 'ARS + '+name.split('_')[4]
                    if name.split('_')[4] == 'ARS':
                        cov = 'ARS'
                    if name.split('_')[4] == 'noagetotyrs':
                        cov = 'RS + totyrs'
                    if 'normstrat' in name:
                        cov = 'ARS + totyrs_normstrat'
                    if 'fgsea' not in name:
                        p = df[df[column+'__padj']<0.05]
                        if len(p) > 0:
                            vals = [column,len(df[df[column+'__pvalue']<0.05]), len(p), len(p[p[column+'__log2FoldChange']>0]), len(p[p[column+'__log2FoldChange']<0]),len(df[df[column+'__pvalue']<0.1]),len(df[df[column+'__padj']<0.01]),len(df[df[column+'__padj']<0.1])]
                            d.writerow([samp, subs, cov, vals[0], vals[1],vals[2],vals[3],vals[4],vals[5],vals[6],vals[7]])
                        else:
                            vals = [column,len(df[df[column+'__pvalue']<0.05]), len(p),0,0,0,0,0]
                            d.writerow([samp, subs, cov, vals[0], vals[1],vals[2],vals[3],vals[4],vals[5],vals[6],vals[7]])
                    else:
                        if column == name.split('_')[-1] or column == name.split('_')[-2]:
                            p = df[df['padj']<0.05]
                            l2fc, gmt = name.split('_')[-1], name.split('_')[-2]
                            if len(p) > 0:
                                vals = [len(df[df['padj']<0.05]), len(df[df['padj']<0.1]), len(p[p['NES']>0]), len(p[p['NES']<0])]
                                f.writerow([samp,subs,cov,l2fc,gmt, vals[0],vals[1],vals[2],vals[3]])
                            else:
                                vals = [len(df[df['padj']<0.05]), len(df[df['padj']<0.1]),0,0]
                                f.writerow([samp,subs,cov,l2fc,gmt, vals[0],vals[1],vals[2],vals[3]])


all_files = sorted(glob.glob('*AtD_*annot*')) + sorted(glob.glob('*ND*annot*')) + sorted(glob.glob('*DementiaHx*annot*')) + \
        sorted(glob.glob('*totyrs*annot*')) + sorted(glob.glob('*afe*annot*')) + sorted(glob.glob('*_AT8_*annot*')) + \
        sorted(glob.glob('*_AR_*annot*')) + sorted(glob.glob('*_NAt_*annot*')) + sorted(glob.glob('*_ARS_*annot*'))

#glob.glob('*AtT*annot*')

to_print(all_files)
