conda install -y -c bioconda -c conda-forge python=3.6 samtools bcftools snpsift plink tabix icu vcftools salmon gcc
#conda install -y -c r -c conda-forge -c bioconda r-base r-logistf readline=6.2
#R -e "source('http://bioconductor.org/biocLite.R'); biocLite(c('DESeq2','GenomeInfoDb','GenomeInfoDbData'))"
pip install -r requirements.txt
